import argparse
import hashlib
import json
import sys
import time

import requests
from rich.live import Live
from rich.table import Table


def parse_args(argv):
    parser = argparse.ArgumentParser('')
    parser.add_argument('url', type=str)
    parser.add_argument('api_key', type=str)
    parser.add_argument('file', type=str)
    parser.add_argument('file_sha', type=str)
    parser.add_argument('exercise', type=str)
    parser.add_argument('language', type=str)
    return parser.parse_args(argv)


def submit(args):
    url = f'{args.url}api/user/submission/'

    payload = {'file_sha': hashlib.sha256(open(args.file,
                                               'rb').read()).hexdigest(),
               'exercise': args.exercise,
               'language': args.language}
    files = [
        ('file', ('raphael.cpp',
                  open(args.file,
                       'rb'), 'application/octet-stream'))
    ]
    headers = {
        'Authorization': f'Api-Key {args.api_key}'
    }

    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    print(response.text)
    return json.loads(response.text)


def jobs_not_finish(args, submission):
    headers = {
        'Authorization': f'Api-Key {args.api_key}'
    }
    jobs = requests.get(f'{args.url}api/user/job?submission={submission.get("id")}',
                        headers=headers).json()

    return not all([j.get('status') != 'PENDING' and j.get('status') != 'RUNNING' for j in jobs])


def jobs(args, submission, live=None):
    table = Table(title=f"Jobs for submission {submission.get('id')}", show_lines=True)
    table.add_column("ID")
    table.add_column("Test Case")

    table.add_column("Status")
    table.add_column("Time ⌚")
    headers = {
        'Authorization': f'Api-Key {args.api_key}'
    }
    jobs = requests.get(f'{args.url}api/user/job?submission={submission.get("id")}',
                        headers=headers).json()

    for j in jobs:
        if 'compile' in j.get('log') and 'tests' in j.get('log'):
            t = json.loads(j.get('log')).get('compile').get('meta').get('time') + \
                json.loads(j.get('log')).get('tests')[0].get('meta').get(
                    'time')
        else:
            t = 0.0

        if j.get('status') == 'OK':
            status = "[green]OK"
        elif j.get('status') == 'RUNNING':
            status = "[blue1]RUNNING"
        elif j.get('status') == 'PENDING':
            status = "[orange]PENDING"
        elif j.get('status') == 'CANCEL':
            status = "[grey27]CANCEL"
        else:
            status = f"[red]{j.get('status')}"
        table.add_row(
            f"{j.get('id')}", j.get('test_case').get('input_name'), status, f'⌚ {t:3.2f}s',
        )

    return table


if __name__ == '__main__':
    args = parse_args(sys.argv[1:])
    submission = submit(args)
    with Live(jobs(args, submission), refresh_per_second=4) as live:
        while jobs_not_finish(args, submission):
            time.sleep(0.4)
            live.update(jobs(args, submission, live))
