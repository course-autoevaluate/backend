# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
import hashlib
import json
import logging
import os
import zipfile

import requests
import yaml
from constance import config
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.hashers import check_password
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.urls import reverse_lazy
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django_downloadview import VirtualDownloadView

from api.models import UserAPIKey
from app.constant import Message
from app.forms import CourseJoinForm, CourseForm, TPDeleteForm, TPForm, ExerciseForm, \
    CourseDeleteForm, TestCaseArchiveForm, SubmitForm, TPArchiveForm, APIKeyForm
from app.models import Course, Participant, TP, Exercise, TestCase, Submission, Job
from app.services import create_test_case_from_archive, create_exo_with_archive, \
    calculate_progress_of_tp, calculate_progress_of_exo_for_participant, create_tp_from_config_file, \
    generate_statement_pdf, calculate_progress_of_exo_for_user
from app.utils import get_tp_configuration_filename

GENERAL_DELETE_HTML = 'general/general-delete.html'


class CourseListView(ListView):
    model = Course
    template_name = 'course/courses.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)

        is_teacher = self.request.user.groups.filter(name='teacher').exists()
        context.update({"course": Course.objects.all(), "is_teacher": is_teacher,
                        "user_courses": Course.objects.filter(participant__user=self.request.user),
                        "segment": "courses", "title": "Courses"})
        return context


class CourseDetailView(DetailView):
    model = Course
    template_name = 'course/course.html'

    def get_context_data(self, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug("get_context_data course detail view ")
        context = super().get_context_data(**kwargs)
        participants = Participant.objects.filter(course=self.object)

        tps = TP.objects.filter(course=self.object)
        all_tps = []
        for tp in tps:
            all_participant = Participant.objects.filter(course=self.object,
                                                         user__groups__name__in=['student'])
            exercises = Exercise.objects.filter(tp=tp)
            if len(all_participant) == 0:
                progress = 0
            else:
                d = calculate_progress_of_tp(exercises, all_participant)
                progress = (sum(d.values()) / len(exercises)) * 100

            all_tps.append({"tp": tp, 'progress': progress})

        context.update({"segment": "course", "title": self.object.name, "tps": all_tps,
                        "participants": participants, "form_archive": TPArchiveForm()})
        return context


class CourseCreateView(LoginRequiredMixin, CreateView):
    model = Course
    template_name = 'course/course-add-edit.html'
    form_class = CourseForm
    success_url = reverse_lazy('courses')
    success_message = 'Course was created successfully'
    error_message = Message.GENERIC_MESSAGE_ERROR

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'title': f"Add course", "action": "Add",
                        "button_action": "Add course", "subtitle": "Add a new course",})
        return context

    def form_valid(self, form):
        form.instance.owner = self.request.user
        messages.success(self.request, self.success_message, 'success')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, self.error_message, 'danger')
        return super().form_invalid(form)


class CourseUpdateView(UpdateView):
    model = Course
    template_name = 'course/course-add-edit.html'
    form_class = CourseForm
    success_url = reverse_lazy('courses')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'title': f"Edit course - {self.object}", "action": "Edit",
                        "button_action": "Save", "subtitle": "Edit course"})
        return context


class CourseDeleteView(DeleteView):
    model = Course
    template_name = GENERAL_DELETE_HTML
    form_class = CourseDeleteForm
    success_url = reverse_lazy('courses')

    def get_context_data(self, **kwargs):
        logger = logging.getLogger(__name__)
        logger.debug("get_context_data course detail view ")
        context = super().get_context_data(**kwargs)
        context.update({"type": "course", "id": self.object.id,
                        'title': f"Delete course"})
        return context


class TPDetailView(DetailView):
    template_name = 'tp/tp.html'
    model = TP

    def get_context_data(self, **kwargs):
        exercises = Exercise.objects.filter(tp=self.object)
        all_participant = Participant.objects.filter(course=self.object.course,
                                                     user__groups__name__in=['student'])
        course = get_object_or_404(Course, pk=self.object.course.id)
        context = super().get_context_data(**kwargs)
        is_teacher = self.request.user.groups.filter(name='teacher').exists()
        if is_teacher:
            all_exercices = []
            d = calculate_progress_of_tp(exercises, all_participant)
            for e, v in zip(exercises, d.values()):
                if len(all_participant) == 0:
                    progress = 0
                else:
                    progress = v * 100
                all_exercices.append({'exo': e, 'progress': progress})

            all_stats = []

            for p in all_participant:
                tmp = {}
                tmp2 = []
                for e in exercises:
                    tmp2.append(calculate_progress_of_exo_for_participant(e, p))
                tmp['user'] = p.user
                tmp['stats'] = tmp2
                all_stats.append(tmp)
            context.update({"type": "tp", "tp": self.object, "id": self.object.id,
                            'title': self.object,
                            'exs': all_exercices if len(all_exercices) > 0 else
                            [{'exo': e, 'progress': 0} for e in exercises], 'course': course,
                            'all_stats': all_stats
                            })
        else:

            all_exs = []
            for e in exercises:
                all_exs.append({'stats': calculate_progress_of_exo_for_user(e, self.request.user),
                                'user': self.request.user, 'exo': e})
            context.update({"type": "tp", "tp": self.object, "id": self.object.id,
                            'title': self.object, 'course': course,
                            'exs': all_exs
                            })
        return context


class TPCreateView(CreateView):
    template_name = 'tp/tp-add-edit.html'
    model = TP
    form_class = TPForm
    success_message = 'TP was created successfully'
    error_message = 'An error has occurred.'

    def get_success_url(self):
        return reverse_lazy('tp', kwargs={'pk': self.object.tp.id})

    def form_valid(self, form):
        form.instance.course = Course.objects.get(pk=self.kwargs.get('pk'))
        result = super().form_valid(form)
        messages.success(self.request, self.success_message, 'success')
        return result

    def form_invalid(self, form):
        messages.error(self.request, self.error_message, 'danger')
        messages.error(self.request, form.errors, 'danger')
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"action": "Add", 'title': f"Add TP",
                        "button_action": "Add",
                        "subtitle": "Add a new TP"})
        return context


class TPUpdateView(UpdateView):
    template_name = 'tp/tp-add-edit.html'
    model = TP
    form_class = TPForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"action": "Edit", "button_action": "Save",
                        'title': f"Edit tp",
                        'subtitle': f'Edit {self.object}'})
        return context


class TPDeleteView(DeleteView):
    template_name = GENERAL_DELETE_HTML
    model = TP
    form_class = TPDeleteForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"type": "tp", "tp": self.object, "id": self.object.id,
                        'title': f"Delete tp"})
        return context


class ExerciseDetailView(DetailView):
    model = Exercise
    template_name = 'tp/ex/ex.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course_id = self.object.tp.course.id
        is_teacher = self.request.user.groups.filter(name='teacher').exists()
        if is_teacher:
            submissions = Submission.objects.filter(exercise=self.object)
            test_cases = TestCase.objects.filter(exercise=self.object.id)
            submission_title = 'All submissions'
        else:
            submissions = Submission.objects.filter(by=self.request.user, exercise=self.object)
            test_cases = TestCase.objects.filter(sample=True, exercise=self.object.id)
            submission_title = 'Your submissions'
        context.update({"type": "ex", "tp": self.object.tp, "id": self.object.id,
                        "course": self.object.tp.course, "ex": self.object,
                        "is_teacher": is_teacher,
                        'title': f"Exercise {self.object.number}", "test_cases": test_cases,
                        "submission_title": submission_title, "submissions": submissions,
                        "form_submit":
                            SubmitForm(initial={"course_id": course_id})})
        return context


class ExerciseCreateView(CreateView):
    model = Exercise
    template_name = 'tp/ex/ex-add.html'
    form_class = ExerciseForm

    def get_success_url(self):
        return reverse_lazy('tp', kwargs={'pk': self.object.tp.id})

    def form_valid(self, form):
        logger = logging.getLogger(__name__)
        logger.debug(self.kwargs)
        form.instance.tp = TP.objects.get(pk=self.kwargs.get('pk'))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"action": "Add", 'title': f"Add exercise",
                        "button_action": "Add",
                        "subtitle": "Add a new exercise"})

        return context


class ExerciseUpdateView(UpdateView):
    model = Exercise
    template_name = 'tp/ex/ex-edit.html'
    form_class = ExerciseForm

    def get_success_url(self):
        return reverse_lazy('ex', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        test_cases = TestCase.objects.filter(exercise=self.object.id)
        context.update({"action": "Edit", 'title': f"Edit exercise",
                        "button_action": "Save",
                        "subtitle_general": "General Information", "course": self.object.tp.course,
                        "subtitle_test": "Test cases", "test_cases": test_cases,
                        "form_archive": TestCaseArchiveForm()})
        return context


class ExerciseDeleteView(DeleteView):
    model = Exercise
    template_name = GENERAL_DELETE_HTML

    def get_success_url(self):
        return reverse_lazy('tp', kwargs={'pk': self.object.exercise.tp.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"type": "exercise", "exercise": self.object, "id": self.object.id,
                        'title': f"Delete exercise"})
        return context


class TestCaseDeleteView(DeleteView):
    model = TestCase
    template_name = GENERAL_DELETE_HTML
    success_message = 'Test case deleted with success'

    def get_success_url(self):
        return reverse_lazy('ex_edit', kwargs={'pk': self.object.exercise.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"type": "test case", "test_case": self.object, "id": self.object.id,
                        'title': f"Delete test case"})
        return context

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()

        self.object.delete()
        messages.success(self.request, self.success_message, 'success')
        return HttpResponseRedirect(success_url)


class StatementDownloadView(VirtualDownloadView):

    def get_mimetype(self):
        return 'application/pdf'

    def get_file(self):
        """Return :class:`django.core.files.base.ContentFile` object."""
        tp_id = self.kwargs.get('pk')
        tp = get_object_or_404(TP, pk=tp_id)
        content = generate_statement_pdf(tp)
        self.attachment = False
        return ContentFile(content, name=f'TP{tp.number}.pdf')


@login_required(login_url="/login/")
@require_http_methods(["GET"])
def index(request):
    context = {'segment': 'index'}

    html_template = loader.get_template('index.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
@require_http_methods(["GET"])
def user_settings(request):
    try:
        api_key = UserAPIKey.objects.get(user=request.user, revoked=False)
    except UserAPIKey.DoesNotExist:
        api_key = None

    return render(request, 'accounts/settings.html',
                  {'api_submission': config.SUBMIT_BY_API, 'current_api_key': api_key,
                   'api_key_form': APIKeyForm()})


@login_required(login_url="/login/")
@require_http_methods(["POST"])
def create_token(request):
    try:
        api_key = UserAPIKey.objects.get(user=request.user, revoked=False)
    except UserAPIKey.DoesNotExist:
        api_key = None

    if api_key is not None:
        return HttpResponseForbidden()
    form = APIKeyForm(request.POST)
    if form.is_valid():
        api_key, key = UserAPIKey.objects.create_key(name=form.cleaned_data['name'],
                                                     user=request.user)

        messages.success(request, f'You\'re api key is: {key}')
        return redirect(reverse_lazy('user-settings'))


@login_required(login_url="/login/")
@require_http_methods(["GET"])
def revoke_token(request):
    api_key = UserAPIKey.objects.get(user=request.user, revoked=False)
    api_key.revoked = True
    api_key.save()

    messages.success(request, f'You\'re api key is revoked ')
    return redirect(reverse_lazy('user-settings'))


@login_required(login_url="/login/")
@require_http_methods(["GET"])
def my_course(request):
    context = {"title": "My courses",
               "course": Course.objects.filter(participant__user=request.user),
               "segment": "my-courses"}
    html_template = loader.get_template('course/courses.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
@require_http_methods(["GET"])
def course_participant_delete(request, pk, user_pk):
    if request.user.has_perm("app.delete_participant"):
        c = get_object_or_404(Course, pk=pk)
        print(c)
        user = get_object_or_404(User, pk=user_pk)
        print(user)
        participant = get_object_or_404(Participant, course=c, user=user)
        participant.delete()
        return redirect('course', pk=c.id)

    return HttpResponseForbidden()


@login_required(login_url="/login/")
@require_http_methods(["GET"])
def course_join(request, pk):
    c = get_object_or_404(Course, pk=pk)
    logger = logging.getLogger(__name__)
    logger.debug(len(c.password))
    if len(c.password) == 0:
        Participant.objects.update_or_create(user=request.user, course=c)
        return redirect('course', pk)

    form = CourseJoinForm(request.POST or None)

    msg = None

    if request.method == "POST":
        if form.is_valid():
            password = form.cleaned_data.get("password")
            if check_password(password, c.password):
                Participant.objects.update_or_create(user=request.user, course=c)
                return redirect('course', pk=c.id)
            else:
                msg = 'Invalid password'
        else:
            msg = 'Error validating the form'

    return render(request, "course/course-join.html",
                  {"form": form, "msg": msg, 'title': f"Join {c.name}"})


@permission_required('app.add_testcase')
@require_http_methods(["POST"])
def post_archive_test_case(request, pk):
    logger = logging.getLogger(__name__)
    logger.debug(pk)
    if request.method == 'POST':
        exercise = get_object_or_404(Exercise, pk=pk)
        form = TestCaseArchiveForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            file = request.FILES.getlist('file')
            with zipfile.ZipFile(file[0], 'r') as zip:
                ok, msg = create_test_case_from_archive(zip, exercise, pk)
                if not ok:
                    messages.error(request, msg)
                else:
                    messages.success(request, 'The test cases have been added with success',
                                     'success')
            return redirect('ex', pk)
        else:
            logger.debug(form.errors)
            messages.error(request, 'An error occurred while adding the test cases.', 'danger')
            messages.error(request, form.errors, 'danger')
            return redirect('ex_edit', pk)


@permission_required('app.add_tp')
@require_http_methods(["POST"])
def post_tp_from_archive(request, pk):
    logger = logging.getLogger(__name__)
    if request.method == 'POST':
        form = TPArchiveForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            file = form.files['files']
            course = get_object_or_404(Course, pk=pk)
            logger.debug(course)
            if request.user not in course.participant_set.all() and request.user != course.owner:
                messages.error(request, "You cannot add a TP to a course that you are not the owner"
                                        "or a member")
                return redirect('courses')

            logger.debug(file)
            with zipfile.ZipFile(file, 'r') as archive:
                top_dirs = set([x.split('/')[0] for x in sorted(archive.namelist()) if
                                x.endswith('/')])
                logger.debug(top_dirs)

                tp_configuration_filename = get_tp_configuration_filename(archive)
                if tp_configuration_filename is None:
                    messages.success(request, f"The config yaml file for TP doesn't exist !")
                    return redirect('course', course.id)

                tp = create_tp_from_config_file(course, archive, tp_configuration_filename)
                for d in top_dirs:
                    create_exo_with_archive(archive, d, tp)
                messages.success(request, f"TP {tp.number} created with success")

            return redirect('course', course.id)


@permission_required('app.add_submission')
@require_http_methods(["POST"])
def submit_solution(request, pk):
    logger = logging.getLogger(__name__)
    logger.debug(pk)
    if request.method == 'POST':
        exercise = get_object_or_404(Exercise, pk=pk)
        form = SubmitForm(request.POST or None, request.FILES or None,
                          initial={'course_id': exercise.tp.course.id})
        if form.is_valid():
            form.instance.by = request.user
            form.instance.exercise = exercise
            tmp = form.instance.file.read()
            form.instance.file_sha = hashlib.sha256(tmp).hexdigest()
            form.instance.save()
            return redirect('ex', pk)
        else:
            logger.debug(form.errors)
            messages.error(request, 'An error occurred while adding the test cases.', 'danger')
            messages.error(request, form.errors, 'danger')
            return redirect('ex_edit', pk)


@permission_required('app.view_testcase')
@require_http_methods(["GET"])
def get_sample(request, pk):
    test_case = get_object_or_404(TestCase, pk=pk)
    is_teacher = request.user.groups.filter(name='teacher').exists()
    if not test_case.sample and not is_teacher:
        messages.error(request, "Forbidden access for this test case")
        return HttpResponseForbidden()
    i, o = test_case.input.read().decode('utf-8'), test_case.output.read().decode('utf-8')
    return render(request, 'modal/testcase.html', context={'input': i, 'output': o})


@permission_required('app.view_submission')
@require_http_methods(["GET"])
def get_source_code(request, pk):
    submission = get_object_or_404(Submission, pk=pk)
    is_teacher = request.user.groups.filter(name='teacher').exists()
    if submission.by != request.user and not is_teacher:
        messages.error(request, "Forbidden access for this submission")
        return HttpResponseForbidden()
    f = submission.file.read().decode('utf-8')
    return render(request, 'modal/sourcecode.html', context={'code': f})


@permission_required('app.view_job')
@require_http_methods(["GET"])
def get_log(request, pk):
    submission = get_object_or_404(Submission, pk=pk)
    is_teacher = request.user.groups.filter(name='teacher').exists()
    if submission.by != request.user and not is_teacher:
        messages.error(request, "Forbidden access for this job")
        return HttpResponseForbidden()

    jobs = Job.objects.filter(~Q(status='CANCEL'), submission=submission)
    logs = []
    for j in jobs:
        if len(j.log) > 0:
            d = json.loads(j.log)
            d.update({'job': j})
            logs.append(d)
    if len(logs) == 0:
        return render(request, 'page-404.html')
    return render(request, 'modal/logs.html', context={'logs': logs})


def page_not_found(request, exception, template_name='page-404.html'):
    html_template = loader.get_template(template_name)
    return HttpResponse(html_template.render({}, request))
