def get_tp_configuration_filename(archive):
    if 'tp.yml' in archive.namelist():
        return 'tp.yml'
    return 'tp.yaml' if 'tp.yaml' in archive.namelist() else None


def get_configuration_filename(root_dir, files):
    if f'{root_dir}/problem.yml' in files:
        return f'{root_dir}/problem.yml'
    return f'{root_dir}/problem.yaml' if f'{root_dir}/problem.yaml' in files else None


def get_current_academic_year():
    import datetime
    now = datetime.datetime.now()

    if now.month >= 9:
        return f'{now.year}-{now.year + 1}'
    else:
        return f'{now.year-1}-{now.year}'