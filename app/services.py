import hashlib
import logging
import os
import subprocess
import tempfile
from distutils.dir_util import copy_tree

import yaml
from constance import config
from django.conf import settings
from django.db.models import Q

from app.constant import SubmissionStatus
from app.models import TestCase, Exercise, Executable, Submission, TP
from app.utils import get_configuration_filename, get_current_academic_year


def _create_test_case_from_file_list(archive, exercise, pk, file_list, root_dir=''):
    for f in file_list:
        sha_sum = []
        for ex in ['in', 'ans']:
            tmp = archive.read(f'{root_dir}{f}.{ex}')
            split = f.split('/')
            filename = split[len(split) - 1]
            with open(f'uploads/test_cases/tp_{exercise.tp.id}'
                      f'_ex_{pk}_{filename}.{ex}', 'wb') as tmp_wr:
                tmp_wr.write(tmp)
                sha_sum.append(hashlib.sha256(tmp).hexdigest())
        is_sample = f.split('/')[0] == 'sample'
        TestCase.objects.update_or_create(exercise=exercise,
                                          input=f'uploads/test_cases/tp_{exercise.tp.id}_ex_{pk}_'
                                                f'{filename}.in',
                                          output=f'uploads/test_cases/tp_{exercise.tp.id}_ex_{pk}_'
                                                 f'{filename}.ans', sample=is_sample,
                                          input_sha=sha_sum[0], output_sha=sha_sum[1])
    return True, 'ok'


def create_test_case_from_archive(archive, exercise, pk):
    logger = logging.getLogger(__name__)
    logger.debug(sorted(archive.namelist()))
    files = sorted(list({el[:-3] if el.endswith('.in') else el[:-4]
                         for el in sorted(archive.namelist()) if
                         not el.endswith('/')}))

    return _create_test_case_from_file_list(archive, exercise, pk, files)


def create_test_case_from_archive_root_dir(archive, exercise, pk, root_dir):
    logger = logging.getLogger(__name__)
    files = sorted(list({el[:-3] if el.endswith('.in') else el[:-4]
                         for el in sorted(archive.namelist()) if
                         not el.endswith('/') and el.startswith(root_dir)}))

    logger.debug('from root_dir')
    logger.debug(files)

    return _create_test_case_from_file_list(archive, exercise, pk, files)


def create_exo_with_archive(archive, root_dir, tp):
    files = [f for f in archive.namelist() if f.startswith(root_dir)]
    logger = logging.getLogger(__name__)
    logger.info("Create the exercise")
    configuration_filename = get_configuration_filename(root_dir, files)
    config = yaml.safe_load(archive.read(configuration_filename))
    logger.debug(config)

    description = archive.read(f'{root_dir}/index.md').decode('utf-8')
    executable = Executable.objects.get(pk=1)
    exo = Exercise.objects.create(number=config.get('number'), description=description,
                                  executable=executable, tp=tp)

    return create_test_case_from_archive_root_dir(archive, exo, exo.pk, f'{root_dir}/data/')


def create_tp_from_config_file(course, archive, tp_configuration_filename):
    config_tp = yaml.safe_load(archive.read(tp_configuration_filename))
    return TP.objects.create(course=course, number=config_tp.get('number'),
                             name=config_tp.get('name'))


def calculate_progress_of_tp(exos, all_participants):
    users = [a.user for a in all_participants]
    d = {}
    if len(all_participants) > 0:
        for e in exos:
            d[e.number] = Submission.objects.filter(exercise=e, status=SubmissionStatus.SUCCESS,
                                                    by__in=users).distinct().count() / len(
                all_participants)
        logger = logging.getLogger(__name__)
        logger.info(d)
    return d


def calculate_progress_of_exo_for_user(exo, user):
    d = {'user': user, 'OK': Submission.objects.filter(exercise=exo,
                                                       status=SubmissionStatus.SUCCESS,
                                                       by=user).distinct().count(),
         'TRIES': Submission.objects.filter(~Q(status=SubmissionStatus.SUCCESS), exercise=exo,

                                            by=user).distinct().count(),
         'ERROR': Submission.objects.filter(status=SubmissionStatus.ERROR, exercise=exo,
                                            by=user).distinct().count(),
         'WRONG_ANSWER': Submission.objects.filter(exercise=exo,
                                                   status=SubmissionStatus.WRONG_ANSWER,
                                                   by=user).distinct().count(),
         'TIMED_OUT': Submission.objects.filter(exercise=exo, status=SubmissionStatus.TIMED_OUT,
                                                by=user).distinct().count(),
         'MEM_OUT': Submission.objects.filter(exercise=exo, status=SubmissionStatus.MEM_OUT,
                                              by=user).distinct().count(),
         'RUNTIME_ERROR': Submission.objects.filter(exercise=exo,
                                                    status=SubmissionStatus.RUNTIME_ERROR,
                                                    by=user).distinct().count()}

    logger = logging.getLogger(__name__)
    logger.info(d)
    return d


def calculate_progress_of_exo_for_participant(exo, p):
    user = p.user
    return calculate_progress_of_exo_for_user(exo, user)


def generate_statement_pdf(tp):
    logger = logging.getLogger(__name__)
    with tempfile.TemporaryDirectory() as tmpdirname:
        copy_tree(os.path.join(settings.BASE_DIR, 'static/pandoc/statement'), tmpdirname)
        logger.debug(tmpdirname)
        list_exo = []
        for e in Exercise.objects.filter(tp=tp):
            with open(f'{tmpdirname}/exo_{e.number}.md', 'w') as f:
                f.write(e.description)
                list_exo.append(f'{tmpdirname}/exo_{e.number}.md')

        sorted(list_exo)

        with open(f'{tmpdirname}/metadata.md', 'r') as f2:
            d = yaml.safe_load(f2)
            d['language'] = config.LANG

            d['institute'] = config.UNIVERSITY
            d['logo'] = f'{settings.BASE_DIR}/../uploads/{config.LOGO_IMAGE}'

            logger.debug(f'{settings.BASE_DIR}/../uploads{config.LOGO_IMAGE}')

            d['degree'] = tp.course.degree
            d['semester'] = tp.course.semester
            d['academic-year'] = get_current_academic_year()

            d['course'] = tp.course.name
            d['acronym'] = tp.course.acronym
            d['type'] = 'TP'
            d['number'] = tp.number
            d['title'] = tp.name
        os.remove(f'{tmpdirname}/metadata.md')
        with open(f'{tmpdirname}/metadata.md', 'w') as f3:
            f3.write('---')
            f3.write('\n')
            yaml.safe_dump(d, f3)
            f3.write('---')
            f3.write('\n')

        base_cmd = f'pandoc --template {tmpdirname}/statement.pandoc --output {tmpdirname}/tp_{tp.number}.pdf {tmpdirname}/metadata.md'.split(
            ' ')
        base_cmd.extend(list_exo)
        proc = subprocess.Popen(base_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()

        logger.info(out)
        logger.error(err)

        return open(f'{tmpdirname}/tp_{tp.number}.pdf', 'rb').read()
