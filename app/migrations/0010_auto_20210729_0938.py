# Generated by Django 3.2.5 on 2021-07-29 09:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_auto_20210729_0938'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='testcase',
            name='input',
        ),
        migrations.RemoveField(
            model_name='testcase',
            name='output',
        ),
    ]
