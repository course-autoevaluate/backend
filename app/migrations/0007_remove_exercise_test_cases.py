# Generated by Django 3.2.5 on 2021-07-29 08:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20210729_0837'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exercise',
            name='test_cases',
        ),
    ]
