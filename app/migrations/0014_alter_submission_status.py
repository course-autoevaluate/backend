# Generated by Django 3.2.5 on 2021-07-29 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20210729_1522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='status',
            field=models.CharField(choices=[('OK', 'OK'), ('TIMEOUT', 'TIMEOUT'), ('MEM_OUT', 'MEM_OUT'), ('RUN_ERROR', 'RUN_ERROR')], default='PENDING', max_length=10),
        ),
    ]
