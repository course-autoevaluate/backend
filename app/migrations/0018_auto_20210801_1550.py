# Generated by Django 3.2.5 on 2021-08-01 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_alter_job_status'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='job',
            options={'ordering': ('test_case',)},
        ),
        migrations.AlterField(
            model_name='job',
            name='status',
            field=models.CharField(choices=[('OK', 'OK'), ('PENDING', 'PENDING'), ('RUNNING', 'RUNNING'), ('COMPILATION_ERROR', 'COMPILATION_ERROR'), ('CANCEL', 'CANCEL'), ('WRONG_ANSWER', 'WRONG_ANSWER'), ('TIMEOUT', 'TIMEOUT'), ('MEM_OUT', 'MEM_OUT'), ('RUN_ERROR', 'RUN_ERROR')], max_length=20),
        ),
    ]
