# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
from django.contrib.auth.decorators import login_required, permission_required
from django.urls import path, re_path
from app import views
from app.views import CourseListView, CourseDetailView, CourseCreateView, CourseUpdateView, \
    CourseDeleteView, TPCreateView, TPDetailView, TPDeleteView, TPUpdateView, ExerciseDetailView, \
    ExerciseDeleteView, ExerciseUpdateView, ExerciseCreateView, TestCaseDeleteView, \
    StatementDownloadView

urlpatterns = [

    # The home page
    path('', views.index, name='home'),
    path('user/', views.user_settings, name='user-settings'),
    path('user/token/create', views.create_token, name='user-token-create'),
    path('user/token/revoke', views.revoke_token, name='user-token-revoke'),
    path('my-courses/', views.my_course, name='my_course'),

    path('courses/', login_required(CourseListView.as_view(), login_url='/login/'), name='courses'),
    path('courses/<int:pk>/', login_required(CourseDetailView.as_view(), login_url="/login/"),
         name='course'),
    path('courses/add/', permission_required('app.add_course')(CourseCreateView.as_view()),
         name='courses_add'),
    path('courses/<int:pk>/edit',
         permission_required('app.change_course')(CourseUpdateView.as_view()),
         name='course_edit'),
    path('courses/<int:pk>/delete',
         permission_required('app.delete_course')(CourseDeleteView.as_view()),
         name='course_delete'),

    path('courses/<int:pk>/join', views.course_join, name='course_join'),
    path('courses/<int:pk>/participant/delete/<int:user_pk>/', views.course_participant_delete,
         name='course_participant_delete'),
    path('courses/<int:pk>/tp/add/', permission_required('app.add_tp')(TPCreateView.as_view()), name='tp_add'),
    path('courses/<int:pk>/tp/addarchive/', views.post_tp_from_archive, name='tp_add_archive'),

    path('tp/<int:pk>/', login_required(TPDetailView.as_view(), login_url="/login/"), name='tp'),
    path('tp/<int:pk>/pdf', StatementDownloadView.as_view(), name='tp_pdf'),
    path('tp/<int:pk>/delete', permission_required('app.delete_tp')(TPDeleteView.as_view()),
         name='tp_delete'),
    path('tp/<int:pk>/edit', permission_required('app.change_tp')(TPUpdateView.as_view()),
         name='tp_edit'),

    path('ex/<int:pk>/add', permission_required('app.add_exercise')(ExerciseCreateView.as_view()),
         name='ex_add'),
    path('ex/<int:pk>/edit',
         permission_required('app.change_exercise')(ExerciseUpdateView.as_view()), name='ex_edit'),
    path('ex/<int:pk>/delete',
         permission_required('app.delete_exercise')(ExerciseDeleteView.as_view()),
         name='ex_delete'),
    path('test_case/<int:pk>/delete',
         permission_required('app.delete_testcase')(TestCaseDeleteView.as_view()),
         name='test_case_delete'),
    path('ex/<int:pk>/testcases', views.post_archive_test_case,
         name='ex_test_case'),
    path('ex/<int:pk>/', login_required(ExerciseDetailView.as_view(), login_url='/login/'),
         name='ex'),
    path('ex/<int:pk>/submit', views.submit_solution,
         name='ex_submit'),

    path('testcase/<int:pk>/', views.get_sample,
         name='testcase'),
    path('submission/<int:pk>/', views.get_source_code,
         name='submission'),

    path('job/<int:pk>/', views.get_log,
         name='log'),

    # Matches any html file
    # re_path(r'^.*\.*', views.pages, name='pages'),

]
