# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin

# Register your models here.
from app.forms import CourseForm
from app.models import Course, TP, Exercise, TestCase, Participant, \
    Executable, Language, Badge, UserBadge


class CourseAdmin(admin.ModelAdmin):
    form = CourseForm


admin.site.register(Course, CourseAdmin)
admin.site.register(TP)
admin.site.register(Exercise)
admin.site.register(TestCase)
admin.site.register(Participant)
admin.site.register(Executable)
admin.site.register(Language)
admin.site.register(Badge)
admin.site.register(UserBadge)
