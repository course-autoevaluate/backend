# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
import logging
import os

from badgrclient import BadgrClient, Assertion
from constance import config
from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_markdown.models import MarkdownField

from core import settings


class Course(models.Model):
    name = models.CharField(max_length=50, unique=True)
    acronym = models.CharField(max_length=10, unique=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    password = models.CharField(max_length=1024, blank=True, null=True, default=None)
    lms_link = models.URLField(default=None)
    available_language = models.ManyToManyField('Language')

    degree = models.CharField(default=None, max_length=50, null=True)
    semester = models.CharField(default=None, max_length=50, null=True)

    def __str__(self):
        return f'{self.pk} - {self.name}'


class Participant(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    date_join = models.DateTimeField(auto_now_add=True, null=True)


class Badge(models.Model):
    image = models.ImageField()
    course = models.CharField(max_length=50)
    other_text = models.CharField(max_length=25)
    description = models.TextField()
    badgr_id = models.CharField(max_length=256, default=None, null=True, blank=True)


class UserBadge(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    badge = models.ForeignKey(Badge, on_delete=models.CASCADE)
    badgr_id = models.CharField(max_length=256, default=None, null=True, blank=True)


class TP(models.Model):
    name = models.CharField(max_length=50)
    number = models.IntegerField(default=1)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    badges = models.ManyToManyField(Badge)
    lms_link = models.URLField(default=None)

    def __str__(self):
        return f"TP {self.number} - {self.name}"

    class Meta:
        unique_together = ('course', 'number',)


class Resource(models.Model):
    name = models.CharField(max_length=256)
    url = models.URLField(default=None, blank=True, null=True)
    file = models.FileField(upload_to=settings.SUBMISSION_RESOURCE_DIRECTORY, default=None, blank=True, null=True)


class Answer(models.Model):
    name = models.CharField(max_length=256)
    value = models.CharField(max_length=256)


class Exercise(models.Model):
    number = models.IntegerField()
    tp = models.ForeignKey(TP, on_delete=models.CASCADE)
    description = MarkdownField()
    executable = models.ForeignKey('Executable', on_delete=models.CASCADE, default=None, blank=True, null=True)
    resources = models.ManyToManyField(Resource)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, default=None, blank=True, null=True)
    type = models.CharField(max_length=20,default='ANSWER', choices=[
        ('ANSWER', 'ANSWER'),
        ('PROGRAM', 'PROGRAM')
    ])


class Executable(models.Model):
    name = models.CharField(max_length=1024)
    file = models.FileField(default=None, null=True,
                            upload_to=settings.SUBMISSION_EXECUTABLES_DIRECTORY)
    build_command = models.TextField()
    file_sha = models.CharField(max_length=256)
    status = models.CharField(max_length=10,
                              choices=[
                                  ('COMPARE', 'COMPARE'),
                                  ('COMPILE', 'COMPILE'),
                                  ('RUN', 'RUN')
                              ]
                              )

    def __str__(self):
        return f'{self.status} - {self.name}'


class Language(models.Model):
    name = models.CharField(max_length=1024)
    allow_submit = models.BooleanField(default=True)
    allow_judge = models.BooleanField(default=True)
    time_factor = models.IntegerField(default=1)
    extensions = models.CharField(max_length=1024)

    def __str__(self):
        return self.name


class SubmissionAnswer(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    value = models.CharField(max_length=256)
    correct = models.BooleanField()


class Submission(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    file = models.FileField(upload_to=settings.SUBMISSION_UPLOAD_DIRECTORY)
    file_sha = models.CharField(max_length=256)
    date_submitted = models.DateTimeField(auto_now_add=True)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    by = models.ForeignKey(User, on_delete=models.CASCADE)
    runner = models.ForeignKey('Runner', on_delete=models.CASCADE, null=True, default=None)
    status = models.CharField(max_length=20, default='PENDING',
                              choices=[
                                  ('SUCCESS', 'SUCCESS'),
                                  ('WRONG_ANSWER', 'WRONG_ANSWER'),
                                  ('COMPILATION_ERROR', 'COMPILATION_ERROR'),

                                  # CAMISOLE OUTPUT
                                  ('TIMED_OUT', 'TIMED_OUT'),
                                  ('MEM_OUT', 'MEM_OUT'),
                                  ('RUNTIME_ERROR', 'RUNTIME_ERROR'),
                                  ('SIGNALED', 'SIGNALED'),

                                  ('ERROR', 'ERROR'),
                                  ('PENDING', 'PENDING'),
                                  ('RUNNING', 'RUNNING')
                              ]
                              )

    class Meta:
        ordering = ('date_submitted',)


class FeedBack(models.Model):
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    by = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField()


class Clarification(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=15, default='OPEN', choices=[
        ('OPEN', 'OPEN'),
        ('CLOSED', 'CLOSED')
    ])


class Message(models.Model):
    clarification = models.ForeignKey
    by = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField()


class Job(models.Model):
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    test_case = models.ForeignKey('TestCase', on_delete=models.CASCADE)
    status = models.CharField(max_length=20,
                              choices=[

                                  ('OK', 'OK'),
                                  ('WRONG_ANSWER', 'WRONG_ANSWER'),
                                  ('PENDING', 'PENDING'),
                                  ('RUNNING', 'RUNNING'),

                                  ('COMPILATION_ERROR', 'COMPILATION_ERROR'),
                                  ('CANCEL', 'CANCEL'),
                                  ('ERROR', 'ERROR'),

                                  # CAMISOLE STATUS
                                  ('TIMED_OUT', 'TIMED_OUT'),
                                  ('MEM_OUT', 'MEM_OUT'),
                                  ('RUNTIME_ERROR', 'RUNTIME_ERROR'),
                                  ('SIGNALED', 'SIGNALED'),
                              ]
                              )
    log = models.TextField()

    class Meta:
        ordering = ('test_case',)


class TestCase(models.Model):
    sample = models.BooleanField()
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    input = models.FileField(upload_to=settings.SUBMISSION_TEST_CASES_DIRECTORY)
    output = models.FileField(upload_to=settings.SUBMISSION_TEST_CASES_DIRECTORY)
    input_sha = models.CharField(max_length=256)
    output_sha = models.CharField(max_length=256)

    @property
    def input_name(self):
        return self.input.name.split('/')[1]


class Runner(models.Model):
    name = models.CharField(max_length=255)
    date_register = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender=Submission, dispatch_uid="create_job")
def create_job(sender, instance: Submission, created, **kwargs):
    if created:
        logger = logging.getLogger(__name__)
        logger.debug(f"Create jobs for submission {instance.id}")

        for test_case in TestCase.objects.filter(exercise=instance.exercise):
            Job.objects.create(submission=instance, test_case=test_case, status='PENDING')
    else:
        if instance.status == 'SUCCESS':
            all_exercise_tp = Exercise.objects.filter(tp=instance.exercise.tp)
            all_submission = Submission.objects.filter(status='SUCCESS', by=instance.by,
                                                       exercise__in=all_exercise_tp)
            if len(all_exercise_tp) == len(all_submission):
                for b in instance.exercise.tp.badges.all():
                    UserBadge.objects.create(badge=b, user=instance.by)


@receiver(post_save, sender=UserBadge, dispatch_uid="create_badge")
def create_job(sender, instance: UserBadge, created, **kwargs):
    if created:
        logger = logging.getLogger(__name__)
        logger.debug(f"Create badge for user_badge {instance.id}")

        client = BadgrClient(os.getenv('BADGR_USERNAME'), os.getenv('BADGR_PASSWORD'),
                             os.getenv('BADGR_CLIENT_iD'), base_url=config.BADGR_URL)

        badge = client.fetch_badgeclass(instance.badge.badgr_id)[0]

        assertion: Assertion = badge.issue(instance.user.email)
        instance.badgr_id = assertion.entityId

        logger.debug(f"Badge created {assertion.entityId}")

        instance.save()


@receiver(models.signals.post_delete, sender=TestCase)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    logger = logging.getLogger(__name__)
    if instance.input and os.path.isfile(instance.input.path):
        os.remove(instance.input.path)
        logger.info(f'File {instance.input.path} removed')
    if instance.output and os.path.isfile(instance.output.path):
        os.remove(instance.output.path)
        logger.info(f'File {instance.output.path} removed')


@receiver(models.signals.pre_save, sender=TestCase)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False
    try:
        old_file_input = TestCase.objects.get(pk=instance.pk).input
        old_file_output = TestCase.objects.get(pk=instance.pk).output
    except TestCase.DoesNotExist:
        return False

    new_file = instance.input
    if not old_file_input == new_file and os.path.isfile(old_file_input.path):
        os.remove(old_file_input.path)

    new_file = instance.output
    if not old_file_output == new_file and os.path.isfile(old_file_output.path):
        os.remove(old_file_output.path)
