from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, PasswordInput, NumberInput, modelformset_factory, \
    inlineformset_factory
from django.forms.utils import ErrorList
from django_markdown.widgets import MarkdownWidget

from api.models import UserAPIKey
from app.models import Course, TP, Language, Exercise, TestCase, Submission, Executable


class APIKeyForm(ModelForm):
    name = forms.CharField(
        help_text=' A free-form name for the API key. Need not be unique. 50 characters max.',
        widget=forms.TextInput(
            attrs={
                "placeholder": "",
                "class": "form-control"
            }
        ))

    class Meta:
        model = UserAPIKey
        exclude = ('user', 'expiry_date', 'revoked')


class CourseForm(ModelForm):
    name = forms.CharField(help_text='The name of the course',
                           widget=forms.TextInput(
                               attrs={
                                   "placeholder": "",
                                   "class": "form-control"
                               }
                           ))
    acronym = forms.CharField(help_text='The acronym of the course (ex: Algorithm => ALG)',
                              widget=forms.TextInput(
                                  attrs={
                                      "placeholder": "",
                                      "class": "form-control"
                                  }
                              ))
    lms_link = forms.URLField(help_text='The LMS platform URL.',
                              widget=forms.URLInput(
                                  attrs={
                                      "placeholder": "",
                                      "class": "form-control"
                                  }
                              ))
    password = forms.CharField(help_text='A password to protect the course. '
                                         'If the password is empty the course is free.',
                               required=False,
                               widget=forms.PasswordInput(
                                   attrs={
                                       "class": "form-control",
                                   }
                               ))
    semester = forms.CharField(help_text='The semester',
                               widget=forms.TextInput(
                                   attrs={
                                       "placeholder": "",
                                       "class": "form-control"
                                   }
                               ))

    degree = forms.CharField(help_text='The degree',
                             widget=forms.TextInput(
                                 attrs={
                                     "placeholder": "",
                                     "class": "form-control"
                                 }
                             ))

    pk = forms.CharField(widget=forms.HiddenInput(), required=False)
    available_language = forms.ModelMultipleChoiceField(queryset=Language.objects.all(), required=False,
                                                        widget=forms.SelectMultiple(attrs={
                                                            "class": "form-control"
                                                        }),
                                                        help_text="Language available "
                                                                  "for this course")

    class Meta:
        model = Course
        exclude = ('free_access', 'owner')


class CourseDeleteForm(ModelForm):
    class Meta:
        model = Course
        fields = ['id']
        widgets = {
            'id': forms.HiddenInput(),
        }


class CourseJoinForm(ModelForm):
    class Meta:
        model = Course
        fields = ['password']
        widgets = {
            'password': PasswordInput(),
        }


class TPDeleteForm(ModelForm):
    class Meta:
        model = TP
        fields = ['id']
        widgets = {
            'id': forms.HiddenInput(),
        }


class TPForm(ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-control"
    }), help_text="The name of the TP")
    number = forms.IntegerField(widget=NumberInput(attrs={
        "class": "form-control"
    }), help_text="TP number")
    lms_link = forms.URLField(help_text='The LMS platform URL.', required=False,
                              widget=forms.URLInput(
                                  attrs={
                                      "placeholder": "",
                                      "class": "form-control"
                                  }
                              ))

    class Meta:
        model = TP
        fields = '__all__'
        exclude = ('badges', 'course')


class ExerciseForm(ModelForm):
    number = forms.IntegerField(widget=NumberInput(attrs={
        "class": "form-control"
    }), help_text="Exercise number")

    description = forms.CharField(widget=MarkdownWidget(attrs={
        "class": "form-control"
    }), help_text="Description")

    executable = forms.ModelChoiceField(queryset=Executable.objects.all(), required=False,
                                        widget=forms.Select(attrs={
                                            "class": "form-control"
                                        }),
                                        help_text="Compare executable")

    type = forms.ChoiceField(choices=[('ANSWER','ANSWER'),('PROGRAM','PROGRAM')], required=False,
                                        widget=forms.Select(attrs={
                                            "class": "form-control"
                                        }),
                                        help_text="")

    class Meta:
        model = Exercise
        fields = ['number', 'description', 'executable', 'type']


class ExerciseDelete(ModelForm):
    class Meta:
        model = Exercise
        fields = ['id']
        widgets = {
            'id': forms.HiddenInput(),
        }


class TestCaseForm(ModelForm):
    input = forms.FileField(widget=forms.ClearableFileInput(attrs={'class': 'inputFileHidden'}))
    output = forms.FileField()

    class Meta:
        model = TestCase
        fields = ('sample', 'input', 'output')


class TestCaseArchiveForm(forms.Form):
    file = forms.FileField()


class TPArchiveForm(forms.Form):
    files = forms.FileField()


class SubmitForm(ModelForm):
    file = forms.FileField()
    language = forms.ModelChoiceField(queryset=Language.objects.all(), empty_label='(Nothing)',
                                      help_text='The language.')

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None,
                 error_class=ErrorList, label_suffix=None, empty_permitted=False, instance=None,
                 use_required_attribute=None, renderer=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix,
                         empty_permitted, instance, use_required_attribute, renderer)
        self.fields['language'].queryset = Language.objects.filter(course=initial['course_id'])

    class Meta:
        model = Submission
        fields = ['file', 'language']
