from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = 'Create the student and teacher group'

    def handle(self, *args, **options):
        teacher, _ = Group.objects.get_or_create(name='teacher')
        print(Permission.objects.all())
        with open('app/management/commands/teacher.lst') as f:
            for l in f.readlines():
                print(l)
                teacher.permissions.add(Permission.objects.get(codename=l.rstrip()))
            teacher.save()

        student, _ = Group.objects.get_or_create(name='student')

        with open('app/management/commands/student.lst') as f:
            for l in f.readlines():
                print(l)
                student.permissions.add(Permission.objects.get(codename=l.rstrip()))
            student.save()
