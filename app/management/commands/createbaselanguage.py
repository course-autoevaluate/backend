from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand

from app.models import Language


class Command(BaseCommand):
    help = 'Create the available language'
    DEFAULT_AVAILABLE_LANGUAGE = ['c++', 'python', 'java', 'haskell']

    def add_arguments(self, parser):
        parser.add_argument('language', nargs='+', type=str)

    def handle(self, *args, **options):
        for l in options['language']:
            obj,_ = Language.objects.get_or_create(name=l)
            self.stdout.write(self.style.SUCCESS('Successfully create "%d"' % obj.id))
