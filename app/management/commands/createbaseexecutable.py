import hashlib

from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand

from app.models import Executable
from core import settings


class Command(BaseCommand):
    help = 'Create the student and teacher group'

    def handle(self, *args, **options):
        with open(f'{settings.MEDIA_ROOT}{settings.SUBMISSION_EXECUTABLES_DIRECTORY}/compare.c', 'r') as f:
            sha_sum = hashlib.sha256(f.read().encode('utf-8')).hexdigest()
            build = """
            #!/bin/sh
             g++ -g -O1 -Wall -fstack-protector -D_FORTIFY_SOURCE=2 -fPIE -Wformat -Wformat-security -ansi -pedantic  -fPIE -Wl,-z,relro -Wl,-z,now  $1 -o $2
            """
            Executable.objects.get_or_create(name='compare',
                                             file=f'{settings.SUBMISSION_EXECUTABLES_DIRECTORY}/compare.c',
                                             status='COMPARE', file_sha=sha_sum,
                                             build_command=build)
