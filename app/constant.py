from enum import Enum


class StrEnum(str, Enum):
    pass


class SubmissionStatus(StrEnum):
    def __new__(cls, value, phrase):
        obj = str.__new__(cls, value)
        obj._value_ = value

        obj.phrase = phrase
        return obj

    SUCCESS = ("SUCCESS", "SUCCESS")
    WRONG_ANSWER = ('WRONG_ANSWER', 'WRONG_ANSWER')
    COMPILATION_ERROR = ('COMPILATION_ERROR', 'COMPILATION_ERROR')

    # CAMISOLE OUTPUT
    TIMED_OUT = ('TIMED_OUT', 'TIMED_OUT')
    MEM_OUT = ('MEM_OUT', 'MEM_OUT')
    RUNTIME_ERROR = ('RUNTIME_ERROR', 'RUNTIME_ERROR')
    SIGNALED = ('SIGNALED', 'SIGNALED')

    ERROR = ('ERROR', 'ERROR')
    PENDING = ('PENDING', 'PENDING')
    RUNNING = ('RUNNING', 'RUNNING')


class Message(Enum):
    GENERIC_MESSAGE_ERROR = 'An error has occurred.'
