FROM python:3.9

COPY manage.py gunicorn-cfg.py requirements.txt .env ./
COPY app app
COPY api api
COPY authentication authentication
COPY core core

RUN pip install -r requirements.txt

RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py createbasegroup
RUN python manage.py createbasegexecutable
RUN python manage.py createbaseglanguage

EXPOSE 5005
CMD ["gunicorn", "--config", "gunicorn-cfg.py", "core.wsgi"]
