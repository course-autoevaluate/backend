# cOUrSE - autOevaluate yoUr Student Easily

![Sonar Quality Gate](https://img.shields.io/sonar/quality_gate/course-autoevaluate_backend?server=https%3A%2F%2Fsonarcloud.io)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=course-autoevaluate_backend&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=course-autoevaluate_backend)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=course-autoevaluate_backend&metric=coverage)](https://sonarcloud.io/dashboard?id=course-autoevaluate_backend)

[![pipeline status](https://gitlab.com/course-autoevaluate/backend/badges/master/pipeline.svg)](https://gitlab.com/course-autoevaluate/backend/-/commits/master)

# Introduction

This project makes it easy to evaluate students automatically. It allows you to create courses, divided into TP, divided into exercises. For each exercise it is possible to upload one or more test cases. 
These test cases can be marked as "sample" so that they can be seen by the students. 

The execution of the programs submitted by the students is done with [camisole](https://camisole.prologin.org/).


# Documentation 

The documentation is [here](https://course-autoevaluate.gitlab.io/). 


# Inspired by its projects

- [DomJudge](https://github.com/DOMjudge/domjudge)

# Original README and CHANGELOG

The base project is [here](https://github.com/creativetimofficial/material-dashboard-django).
The original *README* is [here](ORIGINAL/ORIGINAL.md)
The original *CHANGELOG* is [here](ORIGINAL/ORIGINAL.md)
