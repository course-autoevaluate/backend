# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin
from django.urls import path, include, re_path  # add this

from django.conf.urls import (
    handler400, handler403, handler404, handler500, url
)

urlpatterns = [
    path('admin/', admin.site.urls),  # Django admin route
    re_path('^markdown/', include( 'django_markdown.urls')),
    path("", include("authentication.urls")),  # Auth routes - login / register
    path("", include("app.urls")),  # UI Kits Html files
    path("api/", include("api.urls"))  # UI Kits Html files
]

handler404 = 'app.views.page_not_found'
