# Changelog

This file describes the evolution of the *Statement* template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (December 2019)

+ The language used in the document can be customized from YAML.
+ Informations about the university can be specified from YAML.
+ Informations about the degree can be specified from YAML.
+ Informations about the course can be specified from YAML.
