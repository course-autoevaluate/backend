# Create your views here.
import logging

from django.http import Http404
from django_downloadview import ObjectDownloadView
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from rest_framework_api_key.permissions import HasAPIKey, KeyParser

from api.models import UserAPIKey
from api.perm import HasUserAPIKey
from api.serializer import SubmissionSerializer, JobSerializer, RunnerSerializer, \
    LanguageSerializer, TestCaseSerializer, ExerciseSerializer, ExecutableSerializer, \
    UserSubmissionSerializer
from app.models import Submission, Job, Runner, Language, TestCase, Exercise, Executable


class SubmissionViewSet(viewsets.ModelViewSet):
    queryset = Submission.objects.all()
    serializer_class = SubmissionSerializer
    permission_classes = [HasAPIKey]
    filterset_fields = ['status', 'runner']


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = [HasAPIKey]
    filterset_fields = ['status', 'submission']


class TestCaseViewSet(viewsets.ModelViewSet):
    queryset = TestCase.objects.all()
    serializer_class = TestCaseSerializer
    permission_classes = [HasAPIKey]


class RunnerViewSet(viewsets.ModelViewSet):
    queryset = Runner.objects.all()
    serializer_class = RunnerSerializer
    permission_classes = [HasAPIKey]


class LanguageViewSet(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer
    permission_classes = [HasAPIKey]


class ExerciseViewSet(viewsets.ModelViewSet):
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = [HasAPIKey]


class ExecutableViewSet(viewsets.ModelViewSet):
    queryset = Executable.objects.all()
    serializer_class = ExecutableSerializer
    permission_classes = [HasAPIKey]


class UserSubmissionViewSet(viewsets.ModelViewSet):
    # queryset = Submission.objects.all()
    serializer_class = UserSubmissionSerializer
    parser = [MultiPartParser, FormParser]
    permission_classes = [HasUserAPIKey]

    def get_queryset(self):
        key = KeyParser().get(self.request)
        key_object = UserAPIKey.objects.get_from_key(key)
        if key_object is None:
            raise Http404()
        return Submission.objects.filter(by=key_object.user)

    def perform_create(self, serializer):
        key = KeyParser().get(self.request)
        key_object = UserAPIKey.objects.get_from_key(key)
        if key_object is None:
            raise Http404()
        serializer.validated_data['by'] = key_object.user
        serializer.save()


class UserJobViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = JobSerializer
    permission_classes = [HasUserAPIKey]
    filterset_fields = ['status', 'submission']

    def get_queryset(self):
        key = KeyParser().get(self.request)
        key_object = UserAPIKey.objects.get_from_key(key)
        if key_object is None:
            raise Http404()
        return Job.objects.filter(submission__by=key_object.user)


class MyDownloadView(ObjectDownloadView, APIView):
    permission_classes = api_settings.DEFAULT_PERMISSION_CLASSES


input_test_case = MyDownloadView.as_view(model=TestCase, file_field='input')
output_test_case = MyDownloadView.as_view(model=TestCase, file_field='output')
executable = MyDownloadView.as_view(model=Executable, file_field='file')
submission = MyDownloadView.as_view(model=Submission, file_field='file')
