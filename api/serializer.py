from rest_framework import serializers

from app.models import Submission, Job, Runner, Language, TestCase, Exercise, Executable


class BasicTestCaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCase
        fields = ['id', 'input_name']


class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = '__all__'


class UserSubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        exclude = ['by']


class JobSerializer(serializers.ModelSerializer):
    test_case = BasicTestCaseSerializer(many=False, read_only=True)

    class Meta:
        model = Job
        fields = '__all__'


class RunnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Runner
        fields = '__all__'


class ExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercise
        fields = '__all__'


class ExecutableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Executable
        fields = '__all__'


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'


class TestCaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCase
        fields = '__all__'
