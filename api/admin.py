from django.contrib import admin

# Register your models here.
from rest_framework_api_key.admin import APIKeyModelAdmin

from api.models import UserAPIKey


@admin.register(UserAPIKey)
class UserAPIKeyModelAdmin(APIKeyModelAdmin):
    pass