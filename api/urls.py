from django.urls import include, path
from django_downloadview import ObjectDownloadView
from rest_framework.routers import DefaultRouter

from api import views

router = DefaultRouter()
router.register(r'submission', views.SubmissionViewSet)
router.register(r'job', views.JobViewSet)
router.register(r'runner', views.RunnerViewSet)
router.register(r'language', views.LanguageViewSet)
router.register(r'testcase', views.TestCaseViewSet)
router.register(r'exercise', views.ExerciseViewSet)
router.register(r'executable', views.ExecutableViewSet)
router.register(r'user/submission', views.UserSubmissionViewSet, basename='user-submission')
router.register(r'user/job', views.UserJobViewSet, basename='user-job')

urlpatterns = [
    path('', include(router.urls)),
    path('file/input/<int:pk>', views.input_test_case),
    path('file/output/<int:pk>', views.output_test_case),
    path('file/executable/<int:pk>', views.executable),
    path('file/submission/<int:pk>', views.submission)
]
