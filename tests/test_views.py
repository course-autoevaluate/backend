from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User, Group
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.test import Client
from django.test import TestCase
from django.urls import reverse

from app.management.commands import createbasegroup
from app.models import Course


class DefaultTestCaseView(TestCase):
    def setUp(self) -> None:
        #self.client = Client()

        self.views = None

        cmd = createbasegroup.Command()
        cmd.handle()
        teacher = User.objects.create(username='teacher', password=make_password('teacher'))
        teacher.groups.add(Group.objects.get(name='teacher'))
        teacher.save()

        student = User.objects.create(username='student', password=make_password('student'))
        student.groups.add(Group.objects.get(name='student'))
        student.save()


class TestCourseView(DefaultTestCaseView):
    def setUp(self) -> None:
        super().setUp()
        self.client.logout()
        self.views = [reverse('courses'), reverse('courses_add'),
                      reverse('course_edit', kwargs={'pk': 1}),
                      reverse('course_delete', kwargs={'pk': 1})]

    def test_view_without_login(self):
        for v in self.views:
            r = self.client.get(v)
            self.assertRedirects(r, f'/login/?next={v}')

    def test_view_with_teacher_role(self):
        self.client.login(username='teacher', password='teacher')
        for v in self.views:
            r = self.client.get(v)
            self.assertEqual(200, r.status_code)

    def test_view_with_student_role(self):
        self.client.login(username='student', password='student')
        for v in self.views[1:]:
            r = self.client.get(v)
            self.assertRedirects(r, f'/login/?next={v}')

    def test_delete_participant_without_login(self):
        url = reverse('course_participant_delete', kwargs={'pk': 1, 'user_pk': 1})
        r = self.client.get(reverse('course_participant_delete', kwargs={'pk': 1, 'user_pk': 1}))
        self.assertRedirects(r, f'/login/?next={url}')

    def test_delete_participant_with_student_login(self):
        self.client.login(username='student', password='student')
        print(Course.objects.all())
        r = self.client.get(reverse('course_participant_delete', kwargs={'pk': 1, 'user_pk': 1}))
        self.assertEqual(403, r.status_code)


class TestTPView(DefaultTestCaseView):
    def setUp(self) -> None:
        super().setUp()

        self.views = [reverse('tp', kwargs={'pk': 1}), reverse('tp_add', kwargs={'pk': 1}),
                      reverse('tp_edit', kwargs={'pk': 1}),
                      reverse('tp_delete', kwargs={'pk': 1})]

    def test_view_without_login(self):
        for v in self.views:
            r = self.client.get(v)
            self.assertRedirects(r, f'/login/?next={v}')

    def test_view_with_teacher_role(self):
        self.client.login(username='teacher', password='teacher')
        for v in self.views:
            r = self.client.get(v)
            self.assertEqual(200, r.status_code)

    def test_view_with_student_role(self):
        self.client.login(username='student', password='student')
        for v in self.views[1:]:
            r = self.client.get(v)
            self.assertRedirects(r, f'/login/?next={v}')


class TestExerciseView(DefaultTestCaseView):
    def setUp(self) -> None:
        super().setUp()

        self.views = [reverse('ex', kwargs={'pk': 1}), reverse('ex_add', kwargs={'pk': 1}),
                      reverse('ex_edit', kwargs={'pk': 1}),
                      reverse('ex_delete', kwargs={'pk': 1})]

    def test_view_without_login(self):
        for v in self.views:
            r = self.client.get(v)
            self.assertRedirects(r, f'/login/?next={v}')

    def test_view_with_teacher_role(self):
        self.client.login(username='teacher', password='teacher')
        for v in self.views:
            print(v)
            r = self.client.get(v)
            self.assertEqual(200, r.status_code)

    def test_view_with_student_role(self):
        self.client.login(username='student', password='student')
        for v in self.views[1:]:
            r = self.client.get(v)
            self.assertRedirects(r, f'/login/?next={v}')
