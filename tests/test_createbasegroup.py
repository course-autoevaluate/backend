from django.contrib.auth.models import Group
from django.test import TestCase

from app.management.commands import createbasegroup


class TestCommand(TestCase):
    def test_handle(self):
        cmd = createbasegroup.Command()
        cmd.handle()
        self.assertTrue(Group.objects.count(), 2)
