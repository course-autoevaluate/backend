from django.test import TestCase

from app.management.commands import createbaseexecutable
from app.models import Executable


class TestCommand(TestCase):
    def test_handle(self):
        cmd = createbaseexecutable.Command()
        cmd.handle()
        self.assertTrue(Executable.objects.count(), 1)
