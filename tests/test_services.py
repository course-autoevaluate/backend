from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.test import TestCase

from app.constant import SubmissionStatus
from app.management.commands import createbasegroup, createbaseexecutable
from app.models import Course, TP, Participant, Exercise, Executable, Submission, Language
from app.services import calculate_progress_of_tp
from core import settings


class TestServices(TestCase):
    def setUp(self) -> None:
        cmd = createbasegroup.Command()
        cmd.handle()

        cmd2 = createbaseexecutable.Command()
        cmd2.handle()

        teacher = User.objects.create(username='teacher', password=make_password('teacher'))
        self.student = User.objects.create(username='student', password=make_password('student'))
        self.student2 = User.objects.create(username='student2', password=make_password('student2'))
        self.c = Course.objects.create(name='Algo 1', acronym='ALG1', owner=teacher,
                                       moodle_link='http://test.com')
        self.c.participant_set.add(Participant.objects.create(course=self.c, user=self.student))
        self.c.save()

        self.tp = TP.objects.create(name='TP1', number=1, course=self.c)
        ex, _ = Executable.objects.get_or_create(name='compare',
                                                 file=f'{settings.SUBMISSION_EXECUTABLES_DIRECTORY}'
                                                      f'/compare.c',
                                                 status='COMPARE')
        self.exercise = Exercise.objects.create(
            executable=ex,
            number=1, description="#toto", tp=self.tp)

        self.language = Language.objects.create(name='c++', extensions='cpp')

    def test_calculate_progress_of_tp_without_submission(self):
        self.assertTrue(self.c.participant_set.count() == 1)

        d = calculate_progress_of_tp(Exercise.objects.filter(tp=self.tp),
                                     Participant.objects.filter(course=self.c))

        for k in d.keys():
            self.assertEqual(0, d[k])

    def test_calculate_progress_of_tp_with_submission(self):

        Submission.objects.create(exercise=self.exercise, by=self.student, language=self.language,
                                  status=SubmissionStatus.SUCCESS)

        d = calculate_progress_of_tp(Exercise.objects.filter(tp=self.tp),
                                     Participant.objects.filter(course=self.c))

        for k in d.keys():
            self.assertEqual(1, d[k])

        self.c.participant_set.add(Participant.objects.create(course=self.c, user=self.student2))
        self.c.save()

        Submission.objects.create(exercise=self.exercise, by=self.student2, language=self.language,
                                  status=SubmissionStatus.ERROR)
        d = calculate_progress_of_tp(Exercise.objects.filter(tp=self.tp),
                                     Participant.objects.filter(course=self.c))
        print(d)
        print(Participant.objects.filter(course=self.c))
        for k in d.keys():
            self.assertEqual(0.5, d[k])
