from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.test import TestCase

from app.models import Course, TP


class TestCourseModel(TestCase):

    def test_create_course(self):
        teacher = User.objects.create(username='teacher', password=make_password('teacher'))
        self.assertTrue(Course.objects.count() == 0)
        c = Course.objects.create(name='Algo 1', acronym='ALG1', owner=teacher,
                                  moodle_link='http://test.com')
        self.assertTrue(Course.objects.count() == 1)
        self.assertEqual(c.name, 'Algo 1')


class TestTPModel(TestCase):

    def test_create_tp(self):
        teacher = User.objects.create(username='teacher', password=make_password('teacher'))

        c = Course.objects.create(name='Algo 1', acronym='ALG1', owner=teacher,
                                  moodle_link='http://test.com')

        self.assertTrue(TP.objects.count() == 0)
        TP.objects.create(name='TP1', number=1, course=c)
        self.assertTrue(TP.objects.count() == 1)
